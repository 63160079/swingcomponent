/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.javaswingcomponent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Rattanalak
 */
public class JOptionPaneEx1 {

    JFrame f;

    JOptionPaneEx1() {
        f = new JFrame();
        JOptionPane.showMessageDialog(f, "Hello, Welcome to Javatpoint.");
    }

    public static void main(String[] args) {
        new JOptionPaneEx1();
    }
}
